'use strict';
/**
 *GLOBALS FIRST!
 */
var end_int;
var loc = {};
var searchword = '';
var searchpage = 0;
var lastpage = '';
var currentpage = '';
var locationloaded = false;
var county = '';
var county_code = 0;
var munic = '';
var endless;
var lati = '';
var longi = '';
var googleloaded = false;
var searchurl = '';
var loadnomore;
var noWikiMarkers = [];
var nnWikiMarkers = [];
var meMarker;
var mapinited = false;
var map;
var zoom = 12;
var lastmarker;
var lastApi;
var lastWikiMarker;
var lastWikiLang;
var my_media = null;
var playing = false;
var lastplayed = '';
// Colors for Delving API markers on the map.
var colors = {
  'df': 'FE7569',
  'ks': '9FC6E7'
};
// Colors to highlight last clicked marker.
var hoverColors = {
  'df': 'fe1400',
  'ks': '3a98e7'
};

// Globals for JQ ajax.
$.ajaxSetup({
  beforeSend: function() {
    ajaxAnimate();
  },
  complete: function() {
    ajaxHide();
  }
});

// DEFAULTS FOR ENDLESS SCROLL!
var es_defaults = {
  // Pixel offset from bottom.
  px_offset: 200,
  // Frequency for updating.
  interval_freq: 350,
  // Callback function for loading next page.
  callback: function() {
    $.ajax({
      url: searchurl + searchpage,
      contentType: "application/json",
      success: function(data) {
        delvingRender(data);
        if (!data.result.pagination.hasNext) {
          loadnomore = true;
        }
      }
    });
  },
  // Will stop timer when the following returns true.
  ceaseFire: function() {
    $pane = $('.tab-content .active');
    if ($pane.attr('id') != 'kult') {
      return true;
    }
    if (currentpage != '#searcher') {
      return true;
    }
    return loadnomore;
  }
};

/**
 *Navigation system.
 */
$(window).bind('hashchange', function() {
  // Trigger pagehide on last page.
  $(currentpage).trigger('pagehide');
  // Hide all pages.
  $('.side').hide();
  // Find new page.
  currentpage = window.location.hash;
  // Trigger pageshow.
  $(currentpage).trigger('pageshow');
  // Show page.
  $(currentpage).show();
});
$('#map').live('pageshow', function() {
  // Show a couple of absolute positioned only when map is active.
  $('.text-holder').show();
  $('#zoomer').show();
  // Initialize the map.
  initMap();
});
$('#map').live('pagehide', function() {
  // Hide a couple of absolute positioned elements when map is inactive.
  $('.text-holder').hide();
  $('#zoomer').hide();
});

var apis = [
  {apiName: 'Wikipedia nokmål', apiCode: 'wiki-nb', apiNum: 'wiki-no', checked: true},
  {apiName: 'Wikipedia nynorsk', apiCode: 'wiki-nn', apiNum: 'wiki-nn', checked: false},
  {apiName: 'Digitalt fortalt', apiCode: 'kulturs', apiNum: 'df', checked: false, ksCode: "Digitalt Fortalt"},
  {apiName: 'Kulturhistorisk leksikon', apiCode: 'kulturs', apiNum: 'kl', ksCode: "Kulturhistorisk Leksikon", checked: false},
  {apiName: 'Foto-SF', apiCode: 'kulturs', apiNum: 'fsf', ksCode: 'foto-sf', checked: false},
  {apiName: 'Foto-MR', apiCode: 'kulturs', apiNum: 'fmr', ksCode: 'foto-mr', checked: false},
  {apiName: 'Stadnamn-MR', apiCode: 'kulturs', apiNum: 'smr', ksCode: 'stadnamn-mr', checked: false},
  {apiName: 'Stadnamn-SF', apiCode: 'kulturs', apiNum: 'ssf', ksCode: 'stadnamn-sf', checked: false},
  {apiName: 'Digitalt museum', apiCode: 'kulturs', apiNum: 'dm', ksCode: 'DigitaltMuseum', checked: false}
];

var initTemplates = function() {
  var template = $('#formapis').html();
  $('#data .apiforms').append(Mustache.to_html(template, {apis: apis}));
  $.each(apis, function(i,n) {
    if (n.ksCode) {
      window[n.ksCode] = [];
    }
  });
};

$(document).ready(function() {
  initTemplates();
  $('#zoom-in').click(function() {
    // Use the global variable zoom +1 to zoom in.
    zoom = zoom+1;
    // ...and here we go.
    map.setZoom(zoom);
  });
  $('#zoom-out').click(function() {
    // My very own zoomer, big and functional.
    zoom = zoom-1;
    // Now zooming to global var zoom -1.
    map.setZoom(zoom);
  });
  $('#location-updater').click(function(e) {
    navigator.geolocation.getCurrentPosition(ongeoyea, ongeofail);
    e.preventDefault();
    return false;
  });
  $('#location-inmap').click(function(e) {
    navigator.geolocation.getCurrentPosition(fromMapUpdate, ongeofail);
    e.preventDefault();
    return false;
  });
  // Make the menu items nicely placed.
  var count = $('#navig a').length;
  var percent = 100 / count;
  $("#navig a").css('width', percent + '%');
  var clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
  // Change active classes when things are clicked.
  $('#navig a').bind(clickEventType, function() {
    $('#navig a').addClass('active').not(this).removeClass('active');
  });
  $('.data-marker').live('click', function() {
    // Find marker id.
    var id = $(this).attr('data-id');
    // What datasource is this?
    var api = $(this).attr('data-api');
    // Call the "moreInfo" function on the data of the current marker.
    moreInfo(window[api][id].data, api);
  });
  $('.mp3button').live('click', function() {
    // MP3 url is stored in a fancy HTML5 data attribute of course.
    var mp3file = $(this).attr('data-mp3');
    // Calling player, please play this.
    updateMedia(mp3file);
    if ($(this).hasClass('playing')) {
      // We are already playing, and pressed to stop. Fix classes and icons.
      $(this).removeClass('playing').find('i').removeClass('icon-stop').addClass('icon-music');
    }
    else {
      // Add a playing class and change the icon.
      $(this).addClass('playing').find('i').removeClass('icon-music').addClass('icon-stop');
    }
  });
  $('#selectfylk').change(function() {
    // Find the chosen option.
    if ($(this).val() == '0') {
      // Don't do anything if we have chosen the "placeholder" option.
      return;
    }
    var chosen = $(this).find('option[value="' + $(this).val() + '"]').text();
    // Set global county variable.
    county = chosen;
    // Find the lat/lon from the global county list.
    $.each(allefylker.geonames, function(i,n) {
      if (n.adminName1.indexOf(chosen) > -1) {
        // Set the lat/lon pair as new globals.
        lati = n.lat;
        longi = n.lng;
        // Change the text on the home screen.
        if (locationloaded) {
          // The text is already filled out at least once. Just changee the
          // name span.
          $('.location .fylke').html(county);
        }
        else {
          // We are appending text for the first time.
          $('.location').html('Det ser ut som du befinner deg i <span class="fylke">' + county + '</span>');
        }

        locationloaded = true;
      }
    })
  });
  $('#testern').click(function() {
    $('#modalload').modal();
  });
  $('.searchbutton').click(function(e) {
    $pane = $(this).closest('.tab-pane');
    searchword = $pane.find('.search-val').val();
    $pane.find('.searchwrapper .inner').html('');
    var id = $pane.attr('id');
    if (id == 'wiki') {
      searchMultiWiki();
    }
    else if (id == 'kult') {
      kulturSearch();
    }
    e.preventDefault();
    return false;
  });
});

/**
 * Function to update media (or start a new one.)
 *
 * @param str src
 *   A sound source to start playing.
 */
function updateMedia(src) {
  if (src == lastplayed && playing) {
    // Woah. Media playing, and also last played playing. Must mean we want to
    // stop, eh?
    try {
      my_media.stop();
    } catch(err) {
      // Catch this rascal.
    }
    return;
  }
  lastplayed = src;
  try {
    // Let's try to stop the media.
    my_media.stop();
  } catch(err) {
    // Gotta catch em all.
  }
  my_media = new Media(src, onSuccess, onError, statusChange);
  // Play audio
  my_media.play();
}

/**
 * Void for real.
 */
function onSuccess(data) {
}

/**
 * Void-o-rama.
 */
function onError(error) {
}

/**
 * Callback getting called when state on media changes.
 */
function statusChange(status) {
  if (status == 4) {
    // Player is stopped.
    playing = false;
    return;
  }
  if (status == 1 || status == 2) {
    // Player is either preparing or playing. Either way, let's set playing to
    // true, and let's wait a little since stuff are so extremely async 3.0.
    var t = setTimeout(function() {
      playing = true;
    }, 1000);
  }
}

/**
 * Function to display more info in a new dialog.
 */
var moreInfo = function(data, api) {
  if (api.indexOf('Wiki') > 0) {
    // Yup. Other fields in another API. Who would have thought?
    if (!data) {
      return;
    }
    var title = data.Text.text;
    var desc = data.Description.text;
    var link = data.Url.text.replace('.wikipedia.org', '.m.wikipedia.org');
  }
  else {
    // Go for the "Delving format".
    var title = argornone('dc_title', data, '(Uten tittel)');
    var desc = argornone('dc_description', data, 'Ingen beskrivelse tilgjengelig');
    var link = argornone('europeana_isShownAt', data, '');
  }
  // All of these will return empty anyway for wikis.
  var thumb = argornone('delving_thumbnail', data, '');
  var thisMunic = argornone('abm_municipality', data, '');
  var thisCounty = argornone('abm_county', data, '');
  var author = argornone('dc_creator', data, '');
  var provider = argornone('abm_contentProvider', data, '');
  var mp3 = argornone('europeana_isShownBy', data, '');
  // Make link buttons look nice, if there is data to show.
  mp3 = (mp3.length > 0 && mp3.indexOf('mp3') > 0) ? '<a class="btn btn-success mp3button" href="javascript:void(0)" data-mp3="' + mp3 + '"><i class="icon icon-music"></i> Hør lydklipp</a>' : '';
  link = (link.length > 0) ? '<a class="btn btn-success" href="' + link + '" target="_blank"><i class="icon icon-circle-arrow-right"></i> Les mer</a>' : '';
  // Set title.
  $('#moreinfo h3').html(title);
  // Set image.
  if (thumb.length > 0) {
    $('#dfimg').attr('src', thumb).show();
  }
  else {
    $('#dfimg').hide();
  }
  // Set place to include both municipality and county, if available.
  var place = (thisMunic.length > 0) ? thisMunic + ', ' + thisCounty : thisCounty;
  // Set elements to show, and display text, if text exists only.
  hideIfEmpty('#dftable .place', place);
  hideIfEmpty('#dftable .provider', provider);
  hideIfEmpty('#dftable .link', link);
  hideIfEmpty('#dftable .author', author);
  hideIfEmpty('#dftable .mp3', mp3);
  // Set description.
  $('#dfdesc').html(desc);
  // Go modal, go!
  $('#moreinfo').modal();
}

/**
 * Helper function to add fields, and show (or hide if empty). LAZY!
 */
var hideIfEmpty = function(id, string) {
  if (string.length > 0) {
    $(id).show().find('.append').html(string);
  }
  else {
    $(id).hide();
  }
}

/**
 * Helper function for delving data, to set argument, or a set default.
 *
 * This is also for the lazy.
 */
var argornone = function(arg, data, def) {
  if (arg == 'dc_description') {
    return data[arg] ? '<p>' + data[arg].join('</p><p>') + '</p>' : def;
  }
  return data[arg] ? data[arg][0] : def;
}

/**
 * Function to handle geolocation error.
 */
var ongeofail = function(data) {
  //alert('Fant ikke posisjon på grunn av: ' + data.message);
}

/**
 * Function to handle that the Geolocation was successful.
 */
var ongeoyea = function(position) {
  loc = position.coords;
  lati = position.coords.latitude;
  longi = position.coords.longitude;
  locationloaded = true;
  findCounty();
}

/**
 * Geolocation success callback from the map updater button.
 */
var fromMapUpdate = function(position) {
  lati = position.coords.latitude;
  longi = position.coords.longitude;
  updatemap();
}

/**
 * Function to initialize map, if needed.
 */
var initMap = function() {
  if (locationloaded) {
    if (googleloaded) {
      // Use a timeout, so resize events can finish.
      var t = setTimeout(function() {
        mapInitialize();
      }, 500);
      return;
    }
    // Look, AJAX with no jQuery! How old school!
    var callback = 'mapInitialize';
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=" + callback;
    document.body.appendChild(script);
  }
  else {
    // Phone and/or user is crappy. Darn it!
    alert('Ingen lokasjonsdata tilgjengelig');
  }
}

/**
 * Function to delete layers from the map and from the global scope.
 */
var deleteLayer = function(arrayName) {
  if (!window[arrayName]) {
    return;
  }
  if (window[arrayName].length > 0) {
    console.log(window[arrayName].length);
    for (var i = 0; i < window[arrayName].length; i++) {
      console.log(i);
      window[arrayName][i].setMap(null);
    }
    window[arrayName].length = 0;
    window[arrayName] = [];
  }
}

/**
 * Function to initialize the layers we have chosen on the home screen.
 */
var initLayers = function() {
  lastWikiLang = undefined
  lastWikiMarker = undefined;
  lastmarker = undefined;
  lastApi = undefined;
  var somelayer = false;
  googleloaded = true;
  noWikiMarkers.length > 0 ? deleteLayer('noWikiMarkers') : '';
  nnWikiMarkers.length > 0 ? deleteLayer('nnWikiMarkers') : '';
  $.each(apis, function(i,n) {
    if (n.ksCode && window[n.ksCode].length > 0) {
      deleteLayer(n.ksCode);
    }
  });
  $('.apiforms .layer-group input.inputlayer').each(function(i, n) {
    var active = $(n).attr('checked');
    if (active == 'checked') {
      var layer = $(n).attr('data-callback');
      var kscode = $(n).attr('data-kscode');
      var num = $(n).parent().find('.input-mini').val();
      createLayer(layer, kscode, num);
      somelayer = true;
    }
  });
  if (!somelayer) {
    alert('Ingen kartlag er valgt.');
  }
}

/**
 * Function to create a layer
 */
var createLayer = function(layer, kscode, num) {
  switch(layer) {
    case 'wiki-nb':
      deleteLayer('noWikiMarkers');
      getNearbyWiki('no');
      return;
    case 'wiki-nn':
      deleteLayer('nnWikiMarkers');
      getNearbyWiki('nn');
      return;
    case 'kulturs':
      deleteLayer(kscode);
      getNearbyKS(kscode, num);
      return;
  }
}

/**
 * Function to create an API request against wikilocation.
 */
var getNearbyWiki = function(lang) {
  var url = 'http://api.wikilocation.org/articles?locale=' + lang + '&lat=' + lati + '&lng=' + longi + '&limit=' + $('#wikis-hits').val() + '&radius=' + $('#wiki-' + lang + '-num').val();
  $.ajax({
    url: url,
    success: function(data) {
      wikiLoc(data, lang);
    }
  });
}

/**
 * Callback for the data from wikilocation.
 */
var wikiLoc = function(data, lang) {
  var pinImage = new google.maps.MarkerImage("wiki.png",
    new google.maps.Size(20, 20),
    new google.maps.Point(0, 0),
    new google.maps.Point(16, 16),
    new google.maps.Size(18, 18)
  );
  if (!data) {
    data.length = 0;
  }
  for (var i=0; i<data.articles.length; i++) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(data['articles'][i]['lat'],data['articles'][i]['lng']),
      title: data['articles'][i]['title'],
      icon: pinImage,
      html: '<a href="javascript:void(0)" class="data-marker" data-api="' + lang + 'WikiMarkers' + '" data-id="' + i + '">' + data['articles'][i]['title'] + '</a>'
    });
    window[lang + 'WikiMarkers'].push(marker);
  }
  makeWikiMarkers(lang);
}

/**
 * Function to initialize the map.
 *
 * Sets the center, zoom and so on for each view of the map.
 */
var mapInitialize = function() {
  var lat = lati;
  var lon = longi
  if (mapinited) {
    meMarker.setMap(null);
    meMarker = '';
    google.maps.event.trigger(map, "resize");
    map.setZoom(zoom);
    map.setCenter(new google.maps.LatLng(lati, longi))
  }
  else {
    var point = new google.maps.LatLng(lat, lon);
    var myOptions = {
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: point,
      zoomControl: false,
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
    google.maps.event.addListener(map, 'click', function(event) {
      changePosition(event.latLng);
    });
  }
  mapinited = true;
  var pinColor = "FFF";
  var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
    new google.maps.Size(21, 34),
    new google.maps.Point(0,0),
    new google.maps.Point(10, 34));
  var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
    new google.maps.Size(40, 37),
    new google.maps.Point(0, 0),
    new google.maps.Point(12, 35));
  meMarker = new google.maps.Marker({
    position: new google.maps.LatLng(lat, lon),
    title: 'Ditt valgte punkt',
    icon: pinImage,
    shadow: pinShadow
  });
  meMarker.setMap(map);
  google.maps.event.addListener(meMarker, 'click', function () {
    $('.text-holder').html("Valgt punkt");
  });
  initLayers();
}

/**
 * Change the map position, and retrieve new data.
 */
function changePosition(location) {
  lati = location.lat();
  longi = location.lng();
  updatemap();
}

/**
 * Function to update the map.
 *
 * Gets called either from changing position in the map, or changing position
 * from the map controls
 */
var updatemap = function() {
  noWikiMarkers.length > 0 ? deleteLayer('noWikiMarkers') : '';
  nnWikiMarkers.length > 0 ? deleteLayer('nnWikiMarkers') : '';
  $.each(apis, function(i,n) {
    if (n.ksCode && window[n.ksCode].length > 0) {
      deleteLayer(n.ksCode);
    }
  });
  var t = setTimeout(function() {
    initMap();
  }, 500);
}

/**
 * Function to make the markers we want.
 */
function makeWikiMarkers(lang) {
  for (var i in window[lang + 'WikiMarkers']) {
    window[lang + 'WikiMarkers'][i].setMap(map);
  }
  $.each(window[lang + 'WikiMarkers'], function(i, marker) {
    google.maps.event.addListener(marker, 'click', function () {
      if (lastWikiMarker > -1) {
        window[lastWikiLang + 'WikiMarkers'][lastWikiMarker].setIcon(
          window[lastWikiLang + 'WikiMarkers'][lastWikiMarker].orgIcon
        );
      }
      lastWikiLang = lang;
      lastWikiMarker = i;
      window[lang + 'WikiMarkers'][i].orgIcon = window[lang + 'WikiMarkers'][i].icon;
      window[lang + 'WikiMarkers'][i].setIcon(new google.maps.MarkerImage("wiki2.png",
        new google.maps.Size(20, 20),
        new google.maps.Point(0, 0),
        new google.maps.Point(16, 16),
        new google.maps.Size(20, 20)
      ));
      $.ajax({
        url: 'http://' + lang + '.wikipedia.org/w/api.php?action=opensearch&limit=1&search=' + window[lang + 'WikiMarkers'][i].title + '&format=xml',
        success: function(data) {
          // Convert to json (why is the fullview only in XML???)
          data = $.xml2json(data);
          // Add the data to the marker.
          window[lang + 'WikiMarkers'][i].data = data.Section.Item;
          var description = data.Section.Item ? data.Section.Item.Description.text : '';
          $('.text-holder').html(window[lang + 'WikiMarkers'][i].html + '<p>' + description + '</p>');
        }
      })
    });
  });
}

/**
 * Function used to find current county, based on position.
 */
var findCounty = function() {
  var scriptName = 'http://api.geonames.org/findNearbyJSON?formatted=true&lang=no&lat=' + lati + '&lng=' + longi + '&fclass=P&fcode=PPLA&fcode=PPL&fcode=PPLC&username=eiriksm&style=full&callback=parseCounty'
  var elem = document.createElement ('script');
  elem.setAttribute ('src', scriptName);
  elem.setAttribute ('type','text/javascript');
  document.getElementsByTagName('head')[0].appendChild(elem);
}

/**
 * Function to use the data from the Geonames API.
 */
var parseCounty = function(data) {
  if (!data['geonames']) {
    $('.location').html('Fant koordinatene, men klarte ikke å finne fylke (problem med Geonames sitt API?)');
    locationloaded = false;
    return;
  }
  var navn = '';
  data = data['geonames'][0];
  if (data['adminName1']) {
    navn = data['adminName1'];
  }
  if (navn.length>2) {
    $('.location').html('Det ser ut som du befinner deg i <span class="fylke">' + navn + '</span>');
    county = navn;
  }
  else {
    //$('.location').html('Finner ikke posisjon.');
  }
  if (navn.indexOf("fylke")>1) {
    navn = navn.replace(" fylke", "")
  }
  var kode = $('#selectfylk option:contains(' + navn + ')').val();
  county_code = kode;
  munic = data['adminName2'];
  //findCounty();
}


function kulturSearch() {
  var fylke = '';
  var kommune = '';
  var type = '';
  var url = 'http://kulturnett2.delving.org/organizations/kulturnett/api/search';
  var query = searchword;
  var digital = true;
  if (query.length<1) {
    var query = '*:*';
  }
  var typ = $('#delving .type').val();
  // For later? Right now hardcoding digital objects.
  if (typ.length>1) {
    var type = '&qf=europeana_type_facet:' + typ;
    var digital = false;
  }
  var fylk = $('#delving .fylke').val();
  if (fylk.length>1) {
   var fylke = '&qf=abm_county_facet:' + fylk;
  }
  var side = searchpage;
  var komm = $('#delving .kommune').val();
  if (komm.length>1) {
    var bokst = komm.slice(0,1);
    bokst = bokst.toUpperCase();
    var resten = komm.slice(1);
    var kommune = '&qf=abm_municipality_facet:' + bokst + resten;
  }
  var randomnumber = Math.floor(Math.random()*999);
  var url2 = '&format=json&qf=delving_hasDigitalObject_facet:true' + fylke + kommune + type + '&start=' + side + '&rows=10';
  searchurl = url + '?query=' + query + '&format=json&qf=delving:hasDigitalObject:' + digital + fylke + kommune + type + '&rows=10&start=';
  var url = url + '?query=' + query + url2;
  $.ajax({
    url: url,
    success: function(data) {
      delvingRender(data)
    }
  })
}

/**
 * Create a result list from Delving data.
 *
 * @param object data
 *   Ajax success callback data object.
 */
function delvingRender(data) {
  var found = false;
  searchpage = data.result.pagination.nextPage;
  var url = '';
  $.each(data.result.items, function(i,j) {
    n = j.item.fields;
    var item_county = n['abm_county'] ? n['abm_county'] : '';
    var item_munic = n['abm_municipality'] ? n['abm_municipality'] : '';
    var item_pic = n['delving_thumbnail'] ? n['delving_thumbnail'] : '" style="display:none';
    var desc = n['dc_description'] ? n['dc_description'] : 'Ingen beskrivelse tilgjengelig';
    $('#kult table').append(
      '<tr><td><i class="icon icon-large icon-arrow-right"></i> <img class="" src="' +
      item_pic + '" alt="' + n['dc_title'] + '" title="' +
      n['dc_title'] + '" style="width:80px;float:left;padding:5px"></a><a href="' + n['europeana_isShownAt'] + '" target="_blank"><h5>' +
      n['dc_title'] + '</h5></a>' +
      //'<p>Fylke: ' +  item_county + '. Kommune: ' + item_munic +
      '<span style="font-style:italic;">' + desc + '</span></td></tr>'
    );
  });
  endless = $(window).endlessScroll(es_defaults);
}

/**
 * Function to search multiple wikis.
 */
function searchMultiWiki() {
  var wikis = {
    'wiki-nb': {
      'api': 'http://no.wikipedia.org/w/api.php?action=query&list=search&srsearch=',
      'url': 'http://no.wikipedia.org/wiki/',
      'title': 'Wikipedia Bokmål'
    },
    'wiki-nn': {
      'api': 'http://nn.wikipedia.org/w/api.php?action=query&list=search&srsearch=',
      'url': 'http://nn.wikipedia.org/wiki/',
      'title': 'Wikipedia Nynorsk'
    },
    'wiki-lokal': {
      'api': 'http://lokalhistoriewiki.no/api.php?action=query&list=search&srsearch=',
      'url': 'http://lokalhistoriewiki.no/index.php/',
      'title': 'Lokalhistorie Wiki'
    }
  };
  $.each(wikis, function(i, n) {
    $.ajax({
      url: n.api + searchword + '&format=json',
      success: function(data) {
        $('.' + i).append('<table class="table table-striped table-bordered"><th>Treff i ' + n.title + '</th></table>')
        $.each(data.query.search, function(j, m) {
          $('.' + i + ' table').append('<tr><td><div class="list-row"><h5><a href="' + n.url + m.title + '">' + m.title + '</a></h5><p>' + m.snippet + '</div></td></tr>')
        })
      }
    })
  })
}

/**
 * Getting nearby points from Delving API with this.
 */
var getNearbyKS = function(constrain, num) {
  // var url = 'http://kulturnett2.delving.org/organizations/kulturnett/api/search?query=*:*&qf[]=dcterms_spatial_text:POINT&rows=' + $('#wikis-hits').val() + '&format=json&qf[]=-europeana_collectionTitle_text:sffDf&qf[]=abm_county_facet:' + county;
  var radius = parseInt($('#kulturs-num').val()) / 1000;
  var url = 'http://kulturnett2.delving.org/organizations/kulturnett/api/search?query=*:*&pt=' + lati + ',' + longi + '&rows=' + $('#wikis-hits').val() + '&format=json&qf[]=delving_collection:' + constrain + '&lang=nb&d=' + (num / 1000)
  $('.exclude-group input').each(function(i, n) {
    var active = $(n).attr('checked');
    if (active == 'checked') {
      url += '&' + $(n).attr('data-query');
    }
  });
  $.ajax({
    url: url,
    success: function(data) {
      addDelvingMarkers(data, constrain);
    }
  });
}

/**
 * Function to add the markers from the api response to the map
 */
var addDelvingMarkers = function(data, api) {
  // Proj4js definisjon for WGS84 UTM Sone 33N
  Proj4js.defs["EPSG:32633"] = "+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs";
  var source = new Proj4js.Proj('EPSG:32633');
  var dest = new Proj4js.Proj('WGS84');

  // Different pincolor per source.
  var pinColor = colors[api] || '9FC6E7';
  var pinImage = new google.maps.MarkerImage(api + ".png",
      new google.maps.Size(21, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(10, 34));
  var pinImageMp3 = pinImage;
  $.each(data.result.items, function(i,n) {
    n = n.item.fields;
    if (n.abm_latLong) {
      var mp3 = false;
      // Check if this is an mp3.
      if (api == 'df' && n.europeana_isShownBy && n.europeana_isShownBy[0].indexOf('mp3') > 0) {
        mp3 = true;
      }
      var dc_desc = n['dc_description'] ? n['dc_description'] : '';
      var provider = n['europeana_dataProvider'] ? '(' + n['europeana_dataProvider'] + ')' : '';
      var html = '<i class="icon icon-small ' + typeIcons(n['europeana_type']) + '"></i> <a href="javascript:void(0)" class="data-marker" data-api="' + api + '" data-id="' + i + '">' + n['dc_title'][0] + '</a><p>' + dc_desc + ' <strong>' + provider + '</strong></p>';
      var pin = pinImage;
      if (mp3) {
        pin = pinImageMp3;
        html = html = '<a href="javascript:void(0)" class="btn btn-info btn-small mp3button" data-mp3="' +
        n.europeana_isShownBy + '"><i class="icon icon-music icon-small"></i> </a> <div class="info-wrap"><a href="javascript:void(0)" class="data-marker" data-api="' + api + '" data-id="' + i + '">' + n['dc_title'][0] + '</a><p>' + n['dc_description'] + '</p></div>';
      }
      var latlng = n.abm_latLong[0].split(',');
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latlng[0], latlng[1]),
        title: n['dc_title'][0],
        icon: pin,
        html: html,
        data: n
      });
      window[api].push(marker)
    }
  })

  $.each(window[api], function(i, marker) {
    marker.setMap(map);
    google.maps.event.addListener(marker, 'click', function () {
      $('.text-holder').html(this.html);
      if (lastmarker > -1) {
        window[lastApi][lastmarker].setIcon(window[lastApi][lastmarker].orgIcon);
        window[lastApi][lastmarker].setShadow(window[lastApi][lastmarker].orgShad);
      }
      lastApi = api;
      lastmarker = i;
      this.orgIcon = this.icon;
      this.orgShad = this.shadow;
      var pinColor = hoverColors[api] || '3a98e7';
      this.setIcon(new google.maps.MarkerImage(api + "2.png",
        undefined,
        undefined,
        undefined,
        new google.maps.Size(38, 38)
      ));
    });
  })
}

var translateType = function(string) {
  var strings = {
    'IMAGE': 'Bilde',
    'TEXT': 'Tekst',
    'SOUND': 'Lyd',
    'VIDEO': 'Video'
  }
  return strings[string];
}

var typeIcons = function(string) {
  var strings = {
    'IMAGE': 'icon-picture',
    'TEXT': 'icon-file',
    'SOUND': 'icon-music',
    'VIDEO': 'icon-film'
  }
  return strings[string];
}

/**
 * Function to show an animated gif when loading content with ajax.
 */
var ajaxAnimate = function() {
  $('#ajaxer').show();
}

/**
 * Function to hide the ajax animation when done ajaxing.
 */
var ajaxHide = function() {
  $('#ajaxer').hide();
}
