;(function($) {

    var defaults = {
        px_offset: 50,
        loader: '<br />Loading...<br />',
        insert_after: 'div:last',
        callback: function() { return true; },
        ceaseFire: function() { return false; },
        interval_freq: 150
    };

    function EndlessScroll(scope, options) {
        var _this = this;
        _this.options = $.extend({}, defaults, options);
        _this.scope = scope;
        _this.scroll_id = $(this).attr('id');
        _this.inner_wrap = $('.endless_scroll_inner_wrap', this);

        /* State variables */
        _this.is_scrolling = false;
        _this.stop = false;
        _this.last_pos = 0;
        _this.next_step = 0;

        $(scope).scroll(function() {
            _this.is_scrolling = true;
        });

        return this;
    }

    EndlessScroll.prototype.run = function() {
        var _this = this;
        clearInterval(end_int);
        end_int = setInterval(function() {
            if (!_this.is_scrolling) {
                return;
            }

            if (!_this.is_scrollable()) {
                return;
            }
            if (_this.options.ceaseFire.apply(_this.scope) === true) {
                _this.stop = true;
                return;
            }
            _this.last_pos = $(window).scrollTop();

            //_this.loading();
            _this.request();
            //_this.loading_done();

        }, _this.options.interval_freq);
        return _this;
    };


    EndlessScroll.prototype.loading = function() {
        var _this = this;
        var html = '<div class="endless_scroll_loader_' + _this.scroll_id + ' endless_scroll_loader">' + _this.options.loader + '</div>';
        $(_this.options.insert_after).after(html);
    };

    EndlessScroll.prototype.loading_done = function() {
        var _this = this;
        $('.endless_scroll_loader_' + _this.scroll_id).fadeOut(function() {
            $(this).remove();
        });
    };

    EndlessScroll.prototype.reset_stop = function() {
        this.stop = false;
        this.last_pos = 0;
        return this;
    };

    EndlessScroll.prototype.is_scrollable = function() {
        this.is_scrolling = false;

        var body = document.body, html = document.documentElement;
        var d_height = Math.max(
            body.scrollHeight,
            body.offsetHeight,
            html.clientHeight,
            html.scrollHeight,
            html.offsetHeight
        );
        var w_height = $(window).height();
        var scroll_top = $(window).scrollTop();

        this.next_step = this.last_pos + (w_height/1.80);
        var is_scrollable = d_height - w_height <= scroll_top + this.options.px_offset;
        return (is_scrollable && scroll_top > this.next_step);
    };

    EndlessScroll.prototype.request = function() {
        var _this = this;
        _this.options.callback.apply(_this.scope);
    };

    $.fn.endlessScroll = function(options) {
        return new EndlessScroll(this, options).run();
    };
})(jQuery);
